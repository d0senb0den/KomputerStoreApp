const computersElement = document.getElementById("computers")
const priceElement = document.getElementById("price");
const titleElement = document.getElementById("title");
const descriptionElement = document.getElementById("description");
const specsElement = document.getElementById("specs");
const imageElement = document.getElementById("image");
const buyElement = document.getElementById("purchase")

let computers = []

let salary = 0
let balance = 0
let loanBalance = 0

document.getElementById("balance").innerText = "0 Kr"
document.getElementById("salary").innerText = "0 Kr"
document.getElementById("loanBalance").innerText = "0 Kr"

// Adds 100 SEK to salary
function work(){
    // If you have an active loan, 10% of salary goes to repaying loan
    if(loanBalance != 0){
        salary += 100 * 0.9
        loanBalance -= 100 * 0.1
    }
    else{
        salary += 100
    }
    document.getElementById("salary").innerText = salary + " Kr"
    document.getElementById("loanBalance").innerText = loanBalance + " Kr"
}
// Adds input amount to loan balance
function loan(){
    if(loanBalance == 0){
        const input = prompt("Please enter the amount you want to loan.")
        // Can't be double amount of balance
        if(!(input > balance * 2))
        {
            const loan = parseInt(input)
            balance += loan
            loanBalance += loan
            document.getElementById("balance").innerText = balance + " Kr"
            document.getElementById("loanBalance").innerText = loanBalance + " Kr"
            document.getElementById("repayLoanButton").style.visibility = 'visible'
            document.getElementById("loanText").style.visibility = 'visible'
        }
        else{
            alert("The amount is too large.")
        }
    
    }
    else{
        alert("You may not have two loans at once. The initial loan needs to be paid back in full.")
    }
}
// Repay loan from salary
function repayLoan(){
    const input = prompt("Enter the amount you wish to repay")
    const loan = parseInt(input)
    // Makes sure salary is greater or equal to loan
    if(salary >= loan){
        salary -= loan
        loanBalance -= loan
    }
    else{
        alert("Insufficient funds")
    }
    // Hides button and text if loan balance is 0
    if(loanBalance == 0){
        document.getElementById("repayLoanButton").style.visibility = 'hidden'
        document.getElementById("loanText").style.visibility = 'hidden'
    }
    document.getElementById("salary").innerText = salary + " Kr"
    document.getElementById("loanBalance").innerText = loanBalance + " Kr"
}

function bank(){
    balance += salary
    document.getElementById("balance").innerText = balance + " Kr"
    salary = 0
    document.getElementById("salary").innerText = salary + " Kr"
}

// Takes remove price amount from balance
function purchase(selectedComputer){
    // If balance is greater than or equal to price of pc
    if(balance >= selectedComputer.price){
        balance -= selectedComputer.price
        alert(`Congratulations! You are the new owner of: ${selectedComputer.title}`)
    }
    else{
        alert("Insufficient funds")
    }
    document.getElementById("balance").innerText = balance + " Kr"
}

//response from API
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
.then(response => response.json())
.then(data => computers = data)
.then(computers => addComputersToMenu(computers))

// Shows default values before selecting a computer
const addComputersToMenu = (computers) => {
    computers.forEach(c => addComputerToMenu(c))
    priceElement.innerText = computers[0].price
    titleElement.innerText = computers[0].title
    descriptionElement.innerText = computers[0].description
    imageElement.setAttribute("src", `https://noroff-komputer-store-api.herokuapp.com/${computers[0].image}`)
}

const addComputerToMenu = (computer) => {
    const computerElement = document.createElement("option")
    computerElement.value = computer.id
    computerElement.appendChild(document.createTextNode(computer.title))
    computersElement.appendChild(computerElement)
}

// Shows values when selecting a computer
const handleComputerMenuChange = e => {
    const selectedComputer = computers[e.target.selectedIndex]
    priceElement.innerText = selectedComputer.price
    titleElement.innerText = selectedComputer.title
    descriptionElement.innerText = selectedComputer.description
    imageElement.setAttribute("src", `https://noroff-komputer-store-api.herokuapp.com/${selectedComputer.image}`)
    let specList = ""
    for(let i = 0; i < selectedComputer.specs.length; i++){
        if(i === 0){
            specList = selectedComputer.specs[i]
        }
        else{
            specList += "\n" + selectedComputer.specs[i]
        }
    }
    specsElement.innerText = specList
}

const handlePurchase = () => {
    const selectedComputer = computers[computersElement.selectedIndex]
    purchase(selectedComputer)
}

computersElement.addEventListener("change", handleComputerMenuChange)